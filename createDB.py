import sqlite3
conn = sqlite3.connect('TVintros.db')

try:
	conn.execute("DROP TABLE tblShows")
except:
	pass
conn.execute("""
create table tblShows
(
  TVfile       VarChar,
  TVfilehash   VarChar,
  TVintroStart VarChar,
  TVintroEnd   VarChar,
  constraint tblShows_pk
    unique (TVfile)
);

""")
conn.commit()