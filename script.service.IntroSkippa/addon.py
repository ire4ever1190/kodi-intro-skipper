from kodijson import Kodi, PLAYER_VIDEO
import os
import sqlite3

kodi = Kodi("http://127.0.0.1:8080/jsonrpc")
db_location = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'resources/TVintros.db')
print(db_location)
conn = sqlite3.connect(db_location)
cursor = conn.cursor()
file_path = kodi.Player.GetItem({'playerid': 1, 'properties': ['file']})['result']['item']['file']
file_name = os.path.basename(file_path)
try:
	start, end = cursor.execute('SELECT  TVintroStart, TVintroEnd From tblShows WHERE TVfile = "{}"'.format(file_name)).fetchall()[0]
except Exception as e:
	print(e)
	print(file_name)
else:
	start = int(start)
	end = int(end)
	start_seconds = start % 60
	start_minutes = start // 60
	end_seconds = end % 60
	end_minutes = end // 60
	print(start_seconds, start_minutes, end_seconds, end_minutes)
	current_time = kodi.Player.GetProperties({'playerid': 1, 'properties': ['time']})['result']['time']
	print(current_time)
	if current_time['minutes'] >= start_minutes and current_time['seconds'] >= start_seconds and current_time['minutes'] <= end_minutes and current_time['seconds'] <= end_seconds:
		kodi.Player.Seek({'playerid': 1,'value': {'time': {'hours': 0, 'milliseconds': 0, 'minutes': end_minutes, 'seconds': end_seconds}}})
	else:
		print('not at intro')
