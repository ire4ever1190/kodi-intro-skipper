import cv2
import imagehash
from PIL import Image
import os
import time
from multiprocessing import Pool
from functools import partial
import sqlite3
import hashlib


def find_start_end_frames(video1, video2):
	print('finding start and end with {} and {}'.format(os.path.basename(video1), os.path.basename(video2)))
	fps, hash_array1 = make_hash_array_opencv(video1)
	fps, hash_array2 = make_hash_array_opencv(video2)
	frames = []
	for i in hash_array1:
		for ii in hash_array2:
			if i - ii < 10:
				frames.append(i)

	return frames[0], frames[-1]


def make_hash_array_opencv(video_file, minutes=1):
	cap = cv2.VideoCapture(video_file)
	assert cap.isOpened() is True, 'Not a video file'
	fps = int(round(cap.get(cv2.CAP_PROP_FPS)))
	hash_array = []
	for i in range(fps * 60 * minutes):
		ret, frame = cap.read()
		img = Image.fromarray(frame)
		hash_array.append(imagehash.dhash(img))
		cv2.waitKey(1)
	return fps, hash_array


def find_intro_alt(video, reference_frames):
	conn = sqlite3.connect(os.path.join(os.path.dirname(__file__), 'TVintros.db'))
	hasher = hashlib.md5()
	with open(video, 'rb') as afile:
		buf = afile.read(65536)
		while len(buf) > 0:
			hasher.update(buf)
			buf = afile.read(65536)
	file_hash = str(hasher.hexdigest())
	if conn.execute(f"""SELECT TVfile from tblShows WHERE TVfilehash = '{file_hash}'""").fetchall() != []:
		print('already in database')
		return
	start, end = reference_frames
	fps, browner = make_hash_array_opencv(video)
	frames = 0
	start_time = 0
	stated = False
	finished = []
	THRESHOLD = 8
	for i in browner:
		frames += 1
		if start - i < THRESHOLD and stated is False:
			start_time = frames // fps
			print('started: {}'.format(start_time))
			stated = True
		if end - i < THRESHOLD and stated is True:
			finished.append(frames // fps)
	if finished != []:
		try:
			conn.execute(
				f"""INSERT INTO tblShows VALUES ("{os.path.basename(video)}", '{file_hash}', '{str(start_time)}', '{str(finished[-1])}')""")
		except Exception as e:
			print("+================+")
			print(video)
			print(file_hash)
			print(start_time)
			print(finished[-1])
			print(e)
			print("+================+")
		else:
			conn.commit()
			print('finished: {}'.format(finished[-1]))
	else:
		print("cant find end")
	conn.close()


def scan_current_dir():
	pool = Pool(5)
	start_time = time.perf_counter()
	for root, dirs, files in os.walk('.'):
		videos = []
		for i in files:
			if '.mkv' in i or '.mp4' in i or '.avi' in i:
				videos.append(os.path.join(root, i))

		if len(videos) != 0:
			try:
				reference_frames = find_start_end_frames(videos[1], videos[2])
				pool.map(partial(find_intro_alt, reference_frames=reference_frames), videos)

			except Exception as e:
				print(len(videos))
				print(e)
				pass
	end_time = time.perf_counter()
	print('Start: {} \nFinished: {}'.format(start_time, end_time))


if __name__ == '__main__':
	scan_current_dir()
