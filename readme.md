This find's the intro to a TV show by comparing two episodes and finding similar
frames by using the hashes of the images (dhash).

This method is not 100% effective and gets stuck on things like 'The Simpsons'
something like spliting the audio into small segments would probably work better
but I did not know how to do that.

This method also takes a long time since it reads the video frame by frame using opencv
so it can take a while if you have a lot of episodes

This was more meant as a learning excersie and not a real project but it still can be
useable.

You must make the database yourself and package the database file with the addon by placing the finished 'TVintros.db' file
in script.service.Intro.Skippa/resources and then packaging the addon and installing it

The folder 'kodijson' in the plugin folder is not mine and the original code is from [here](https://github.com/jcsaaddupuy/python-kodijson)